<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211017144350 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('ALTER TABLE place ADD sub_category_place_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE place ADD handle_for_description VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE place ALTER street_number DROP NOT NULL');
        $this->addSql('ALTER TABLE place ALTER street_name DROP NOT NULL');
        $this->addSql('ALTER TABLE place ALTER address_complement DROP NOT NULL');
        $this->addSql('ALTER TABLE place ALTER phone_number DROP NOT NULL');
        $this->addSql('ALTER TABLE place ALTER email DROP NOT NULL');
        $this->addSql('ALTER TABLE place ALTER website DROP NOT NULL');
        $this->addSql('ALTER TABLE place ALTER timetable DROP NOT NULL');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CDF8C57588 FOREIGN KEY (sub_category_place_id) REFERENCES sub_category_place (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_741D53CDF8C57588 ON place (sub_category_place_id)');
        $this->addSql('ALTER TABLE sub_category_place ADD category_place_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sub_category_place ADD CONSTRAINT FK_FD8A4E90BADC2662 FOREIGN KEY (category_place_id) REFERENCES category_place (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_FD8A4E90BADC2662 ON sub_category_place (category_place_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('ALTER TABLE sub_category_place DROP CONSTRAINT FK_FD8A4E90BADC2662');
        $this->addSql('DROP INDEX IDX_FD8A4E90BADC2662');
        $this->addSql('ALTER TABLE sub_category_place DROP category_place_id');
        $this->addSql('ALTER TABLE place DROP CONSTRAINT FK_741D53CDF8C57588');
        $this->addSql('DROP INDEX IDX_741D53CDF8C57588');
        $this->addSql('ALTER TABLE place DROP sub_category_place_id');
        $this->addSql('ALTER TABLE place DROP handle_for_description');
        $this->addSql('ALTER TABLE place ALTER street_number SET NOT NULL');
        $this->addSql('ALTER TABLE place ALTER street_name SET NOT NULL');
        $this->addSql('ALTER TABLE place ALTER address_complement SET NOT NULL');
        $this->addSql('ALTER TABLE place ALTER phone_number SET NOT NULL');
        $this->addSql('ALTER TABLE place ALTER email SET NOT NULL');
        $this->addSql('ALTER TABLE place ALTER website SET NOT NULL');
        $this->addSql('ALTER TABLE place ALTER timetable SET NOT NULL');
    }
}
