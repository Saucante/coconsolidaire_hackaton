<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211017140513 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE applicant_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE category_place_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE city_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE place_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE sub_category_place_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE applicant (id INT NOT NULL, handle VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE category_place (id INT NOT NULL, handle VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE city (id INT NOT NULL, handle VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE place (id INT NOT NULL, city_id INT DEFAULT NULL, sub_category_place_id INT DEFAULT NULL, handle_for_name VARCHAR(100) NOT NULL, handle_for_description VARCHAR(100) NOT NULL, street_number VARCHAR(100) DEFAULT NULL, street_name VARCHAR(100) DEFAULT NULL, address_complement VARCHAR(100) DEFAULT NULL, phone_number VARCHAR(100) DEFAULT NULL, email VARCHAR(100) DEFAULT NULL, website VARCHAR(100) DEFAULT NULL, is_accessible BOOLEAN NOT NULL, is_validated BOOLEAN NOT NULL, timetable VARCHAR(500) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_741D53CD8BAC62AF ON place (city_id)');
        $this->addSql('CREATE INDEX IDX_741D53CDF8C57588 ON place (sub_category_place_id)');
        $this->addSql('CREATE TABLE sub_category_place (id INT NOT NULL, category_place_id INT DEFAULT NULL, handle VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FD8A4E90BADC2662 ON sub_category_place (category_place_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CD8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CDF8C57588 FOREIGN KEY (sub_category_place_id) REFERENCES sub_category_place (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sub_category_place ADD CONSTRAINT FK_FD8A4E90BADC2662 FOREIGN KEY (category_place_id) REFERENCES category_place (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE sub_category_place DROP CONSTRAINT FK_FD8A4E90BADC2662');
        $this->addSql('ALTER TABLE place DROP CONSTRAINT FK_741D53CD8BAC62AF');
        $this->addSql('ALTER TABLE place DROP CONSTRAINT FK_741D53CDF8C57588');
        $this->addSql('DROP SEQUENCE applicant_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE category_place_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE city_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE place_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE sub_category_place_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP TABLE applicant');
        $this->addSql('DROP TABLE category_place');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE place');
        $this->addSql('DROP TABLE sub_category_place');
        $this->addSql('DROP TABLE "user"');
    }
}
