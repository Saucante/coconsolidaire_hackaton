<?php

namespace App\DataFixtures;

use App\Entity\City;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


class CityFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $city = new City();
        $city->setHandle("Lyon");
        $manager->persist($city);
        $manager->flush();
    }
}
