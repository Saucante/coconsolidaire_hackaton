<?php

namespace App\DataFixtures;

use App\Entity\Place;
use App\Repository\SubCategoryPlaceRepository;
use App\Repository\CityRepository;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PlaceFixtures extends Fixture implements DependentFixtureInterface
{


    /**
     * @var SubCategoryPlaceRepository
     */
    protected $subCategoryPlaceRepository;

    /**
     * @var CityRepository
     */
    protected $cityRepository;


    // Ici, on va se servir des tags qu'on a déjà inséré en base (pas obligatoire ;) )

    public function __construct(
        SubCategoryPlaceRepository $subCategoryPlaceRepository,
        CityRepository $cityRepository
    ) {
        $this->subCategoryPlaceRepository = $subCategoryPlaceRepository;
        $this->cityRepository = $cityRepository;
    }


    public function load(ObjectManager $manager)
    {


        $place = new Place();

        $subCategory = $this->subCategoryPlaceRepository->findOneBy(array('handle' => 'Education'));
        $place->setSubCategoryPlace($subCategory);

        $cityLyon = $this->cityRepository->findOneBy(array('handle' => 'Lyon'));
        $place->setCity($cityLyon);

        $place->setHandleForName('Asso trop chouette');
        $place->setHandleForDescription('Description de l\'asso trop chouette');

        $place->setIsAccessible(false);
        $place->setIsValidated(true);


        $manager->persist($place);
        $manager->flush();

    }


    public function getDependencies()
    {
        return [
            CategoryPlaceFixtures::class,
            CityFixtures::class,
        ];
    }
}
