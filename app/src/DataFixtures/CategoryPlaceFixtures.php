<?php

namespace App\DataFixtures;

use App\Entity\CategoryPlace;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


class CategoryPlaceFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $categoryPlaceNames = [
            'Santé',
            'Parentalité',
            'Education et recherche',
            'Logement',
            'Emploi',
            'Droit',
            'Politique et associations militantes',
            'Culture',
            'Loisirs',
            'Sport',
            'Safe place and tools',
            'Communauté',
        ];

        foreach ($categoryPlaceNames as $categoryPlaceName) {
            $categoryPlace = new CategoryPlace();
            $categoryPlace->setHandle($categoryPlaceName);
            $manager->persist($categoryPlace);
        }

        $manager->flush();
    }
}