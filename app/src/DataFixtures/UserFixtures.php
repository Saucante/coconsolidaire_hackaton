<?php

namespace App\DataFixtures;

use App\Entity\User;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{

    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $passwordHasher;

    // Ici, on va se servir des tags qu'on a déjà inséré en base (pas obligatoire ;) )

    public function __construct(
        UserPasswordHasherInterface $passwordHasher
    ) {
        $this->passwordHasher = $passwordHasher;
    }


    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setEmail("lecoconsolidaire@email.com");
        $admin->setPassword(
            $this->passwordHasher->hashPassword($admin, '123456')
        );

        $manager->persist($admin);
        $manager->flush();

    }
}
