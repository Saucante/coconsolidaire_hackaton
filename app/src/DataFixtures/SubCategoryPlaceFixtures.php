<?php

namespace App\DataFixtures;

use App\Entity\SubCategoryPlace;
use App\Repository\CategoryPlaceRepository;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class SubCategoryPlaceFixtures extends Fixture implements DependentFixtureInterface
{


    /**
     * @var CategoryPlaceRepository
     */
    protected $categoryPlaceRepository;


    // Ici, on va se servir des tags qu'on a déjà inséré en base (pas obligatoire ;) )

    public function __construct(
        CategoryPlaceRepository $categoryPlaceRepository
    ) {
        $this->categoryPlaceRepository = $categoryPlaceRepository;
    }



    public function load(ObjectManager $manager)
    {

        $subCategoryPlaceNamesSante = [
            'Santé',
            'Violence',
            'Addiction',
        ];

        $subCategoryPlaceNamesEducationEtRecherche = [
            'Education',
            'Recherche',
        ];

        $subCategoryPlaceNamesLogement = [
            'Général-logement',
            'Sans-abrisme',
        ];

        $subCategoryPlaceNamesEmploi = [
            'Réseau et associations',
            'Entrepreneuriat',
            'syndicats',
        ];

        $subCategoryPlaceNamesDroit = [
            'Général-droit',
            'Associations féminines',
            'Associations féministes',
        ];

        $subCategoryPlaceNamesCommunaute = [
            'LGBTQIA+',
            'Migrants',
            'Filles et jeunes mères',
            'Mères, enfants et pères',
            'Ainés féminins',
        ];

        $categoryPlaceSante = $this->categoryPlaceRepository->findOneBy(array('handle' => 'Santé'));
        $categoryPlaceEducationEtRecherche = $this->categoryPlaceRepository->findOneBy(array('handle' => 'Education et recherche'));
        $categoryPlaceLogement = $this->categoryPlaceRepository->findOneBy(array('handle' => 'Logement'));
        $categoryPlaceEmploi = $this->categoryPlaceRepository->findOneBy(array('handle' => 'Emploi'));
        $categoryPlaceDroit = $this->categoryPlaceRepository->findOneBy(array('handle' => 'Droit'));
        $categoryPlaceCommunaute = $this->categoryPlaceRepository->findOneBy(array('handle' => 'Communauté'));

        foreach ($subCategoryPlaceNamesSante as $subCategoryPlaceName) {
            $subCategoryPlace = new SubCategoryPlace();
            $subCategoryPlace->setHandle($subCategoryPlaceName);
            $subCategoryPlace->setCategoryPlace($categoryPlaceSante);
            $manager->persist($subCategoryPlace);
        }


        foreach ($subCategoryPlaceNamesEducationEtRecherche as $subCategoryPlaceName) {
            $subCategoryPlace = new SubCategoryPlace();
            $subCategoryPlace->setHandle($subCategoryPlaceName);
            $subCategoryPlace->setCategoryPlace($categoryPlaceEducationEtRecherche);
            $manager->persist($subCategoryPlace);
        }

        
        foreach ($subCategoryPlaceNamesEmploi as $subCategoryPlaceName) {
            $subCategoryPlace = new SubCategoryPlace();
            $subCategoryPlace->setHandle($subCategoryPlaceName);
            $subCategoryPlace->setCategoryPlace($categoryPlaceLogement);
            $manager->persist($subCategoryPlace);
        }


        foreach ($subCategoryPlaceNamesLogement as $subCategoryPlaceName) {
            $subCategoryPlace = new SubCategoryPlace();
            $subCategoryPlace->setHandle($subCategoryPlaceName);
            $subCategoryPlace->setCategoryPlace($categoryPlaceEmploi);
            $manager->persist($subCategoryPlace);
        }


        foreach ($subCategoryPlaceNamesDroit as $subCategoryPlaceName) {
            $subCategoryPlace = new SubCategoryPlace();
            $subCategoryPlace->setHandle($subCategoryPlaceName);
            $subCategoryPlace->setCategoryPlace($categoryPlaceDroit);
            $manager->persist($subCategoryPlace);
        }

        foreach ($subCategoryPlaceNamesCommunaute as $subCategoryPlaceName) {
            $subCategoryPlace = new SubCategoryPlace();
            $subCategoryPlace->setHandle($subCategoryPlaceName);
            $subCategoryPlace->setCategoryPlace($categoryPlaceCommunaute);
            $manager->persist($subCategoryPlace);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryPlaceFixtures::class,
        ];
    }
}

