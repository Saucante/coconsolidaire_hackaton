<?php

namespace App\Entity;

use App\Repository\SubCategoryPlaceRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=SubCategoryPlaceRepository::class)
 */
class SubCategoryPlace
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $handle;

    /**
     * @ORM\ManyToOne(targetEntity=CategoryPlace::class, inversedBy="subCategoriesplace", fetch="EAGER")
     */
    private $categoryPlace;


    /**
     * @ORM\OneToMany(targetEntity=Place::class, mappedBy="subCategoryPlace", fetch="EAGER")
     */
    private $places;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHandle(): ?string
    {
        return $this->handle;
    }

    public function setHandle(string $handle): self
    {
        $this->handle = $handle;

        return $this;
    }

    public function getCategoryPlace(): ?CategoryPlace
    {
        return $this->categoryPlace;
    }

    public function setCategoryPlace(?CategoryPlace $categoryPlace): self
    {
        $this->categoryPlace = $categoryPlace;

        return $this;
    }

            /**
     * @return Collection|Place[]
     */
    public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Place $place): self
    {
        if (!$this->places->contains($place)) {
            $this->places[] = $place;
        }

        return $this;
    }
}
