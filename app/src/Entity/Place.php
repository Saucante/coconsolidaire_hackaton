<?php

namespace App\Entity;

use App\Repository\PlaceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlaceRepository::class)
 */
class Place
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $handleForName;


        /**
     * @ORM\Column(type="string", length=100)
     */
    private $handleForDescription;


    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $streetNumber;

        /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $streetName;

        /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $addressComplement;

        /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phoneNumber;

        /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $email;
    
            /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $website;

            /**
     * @ORM\Column(type="boolean")
     */
    private $isAccessible;

            /**
     * @ORM\Column(type="boolean")
     */
    private $isValidated;

            /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $timetable;


    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="places", fetch="EAGER")
     */
    private $city;


    /**
     * @ORM\ManyToOne(targetEntity=SubCategoryPlace::class, inversedBy="places", fetch="EAGER")
     */
    private $subCategoryPlace;


    public function __construct()
    {
        $this->isValidated = false;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHandleForName(): ?string
    {
        return $this->handleForName;
    }

    public function setHandleForName(string $handleForName): self
    {
        $this->handleForName = $handleForName;

        return $this;
    }

    public function getHandleForDescription(): ?string
    {
        return $this->handleForDescription;
    }

    public function setHandleForDescription(string $handleForDescription): self
    {
        $this->handleForDescription = $handleForDescription;

        return $this;
    }
    
    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(string $streetNumber): self
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getAddressComplement(): ?string
    {
        return $this->addressComplement;
    }

    public function setAddressComplement(string $addressComplement): self
    {
        $this->addressComplement = $addressComplement;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getIsAccessible(): ?bool
    {
        return $this->isAccessible;
    }

    public function setIsAccessible(bool $isAccessible): self
    {
        $this->isAccessible = $isAccessible;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    public function getTimetable(): ?string
    {
        return $this->timetable;
    }

    public function setTimetable(string $timetable): self
    {
        $this->timetable = $timetable;

        return $this;
    }

    
    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getSubCategoryPlace(): ?SubCategoryPlace
    {
        return $this->subCategoryPlace;
    }

    public function setSubCategoryPlace(?SubCategoryPlace $subCategoryPlace): self
    {
        $this->subCategoryPlace = $subCategoryPlace;

        return $this;
    }

}
