<?php

namespace App\Entity;

use App\Repository\CategoryPlaceRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=CategoryPlaceRepository::class)
 */
class CategoryPlace
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $handle;

    /**
     * @ORM\OneToMany(targetEntity=SubCategoryPlace::class, mappedBy="categoryPlace", fetch="EAGER")
     */
    private $subCategoriesplace;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHandle(): ?string
    {
        return $this->handle;
    }

    public function setHandle(string $handle): self
    {
        $this->handle = $handle;

        return $this;
    }

            /**
     * @return Collection|SubCategoryPlace[]
     */
    public function getSubCategoriesPlace(): Collection
    {
        return $this->subCategoriesplace;
    }

    public function addSubCategoryPlace(SubCategoryPlace $subCategoryPlace): self
    {
        if (!$this->subCategoriesplace->contains($subCategoryPlace)) {
            $this->subCategoriesplace[] = $subCategoryPlace;
        }

        return $this;
    }
}
