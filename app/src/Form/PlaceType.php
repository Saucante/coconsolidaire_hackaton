<?php

namespace App\Form;

use App\Entity\Place;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('handleForName', TextType::class)
            ->add('handleForDescription', TextType::class)
            ->add('streetNumber', TextType::class)
            ->add('streetName', TextType::class)
            ->add('addressComplement', TextType::class)
            ->add('phoneNumber', TextType::class)
            ->add('email', TextType::class)
            ->add('website', TextType::class)
            ->add('timetable', TextType::class)
            ->add('subCategoryPlace', EntityType::class, [
                 'class' => SubCategoryPlace::class,
                 'choice_label' => 'nom',
                 'placeholder' => 'Filtrer par campus...',
                 'label' => 'Par Campus',
                 'required'=> false
             ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Place::class,
        ]);
    }
}
