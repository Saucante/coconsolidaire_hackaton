<?php

namespace App\Controller;

use App\Repository\CategoryPlaceRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class PublicController extends AbstractController
{
    /**
     * @var CategoryPlaceRepository
     */
    protected $categoryPlaceRepository;

    /**
     * @Route("/", name="home")
     */
    public function index(
        CategoryPlaceRepository $categoryPlaceRepository
    ): Response
    {
        $categories = $categoryPlaceRepository->findAll();
        return $this->render('public/index.html.twig', [
            'controller_name' => 'PublicController',
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/lecocon", name="asso")
    */
    public function asso(): Response
    {
        return $this->render('public/asso.html.twig', [
            'controller_name' => 'PublicController',
        ]);
    }

    /**
     * @Route("/emergency", name="emergency")
    */
    public function emergency(): Response
    {
        return $this->render('public/emergency.html.twig', [
            'controller_name' => 'PublicController',
        ]);
    }
}
