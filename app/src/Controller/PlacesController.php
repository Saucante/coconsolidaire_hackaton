<?php

namespace App\Controller;

use App\Entity\Place;
use App\Form\PlaceType;
use App\Repository\CategoryPlaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("/places", name="places_")
*/
class PlacesController extends AbstractController
{
    /**
     * @Route("category/{categoryHandle}", name="category")
    */
    public function category(
        string $categoryHandle,
        CategoryPlaceRepository $categoryRepository
        ): Response
    {
        $category = $categoryRepository->findOneByHandle($categoryHandle);
        return $this->render('public/places/category.html.twig', [
            "category" => $category,
            'controller_name' => 'PublicController',
        ]);
    }

    /**
         * @Route("/add", name="add")
        */
        public function add(
            Request $request,
            EntityManagerInterface $entityManager
        ): Response
        {
            $place = new Place();
            $placeType = $this->createForm(PlaceType::class, $place);

            $place->setCity(null);
            $place->setSubCategoryPlace(null);

            $placeType->handleRequest($request);

            if($placeType->isSubmitted() && $placeType->isValid())
            {
                $entityManager->persist($place);
                $entityManager->flush();
                $this->addFlash('success', "Le lieu a été crée avec succès.");
                return $this->redirectToRoute('home');
            }

            return $this->render('public/places/add.html.twig', [
                'placeType' => $placeType->createView(),
            ]);
        }

    /**
     * @Route("/{id}", name="single")
    */
    public function single(
        int $id,
        PlaceRepository $placeRepository
        ): Response
    {
        $place = $placeRepository->find($id);
        return $this->render('public/places/single.html.twig', [
            "place" => $place,
            'controller_name' => 'PublicController',
        ]);
    }
}
