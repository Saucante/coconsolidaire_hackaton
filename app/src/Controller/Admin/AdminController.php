<?php

namespace App\Controller\Admin;

use App\Entity\Place;
use App\Entity\CategoryPlace;
use App\Entity\SubCategoryPlace;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class AdminController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        // redirect to some CRUD controller
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(PlaceCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Le cocon solidaire');
    }

    public function configureMenuItems(): iterable
    {
        return [
        MenuItem::section('Lieux'),
        MenuItem::linkToCrud('Lieux', 'fa fa-users-cog', Place::class),

        MenuItem::section('Categories'),
        MenuItem::linkToCrud('Categories', 'fa fa-users-cog', CategoryPlace::class),

        MenuItem::section('Sous-Categories'),
        MenuItem::linkToCrud('Sous-Categories', 'fa fa-users-cog', SubCategoryPlace::class),

        MenuItem::section('Utilisateurs'),
        MenuItem::linkToCrud('Utilisateurs', 'fa fa-users-cog', User::class),
        ];
    }
}
