<?php

namespace App\Controller\Admin;

use App\Entity\SubCategoryPlace;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SubCategoryPlaceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SubCategoryPlace::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
